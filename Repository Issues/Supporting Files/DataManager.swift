//
//  DataManager.swift
//  Repository Issues
//
//  Created by Durjay Singha on 05/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class DataManager {
    static let shared = DataManager(moc:NSManagedObjectContext.current)
    var managedContext: NSManagedObjectContext
    private init(moc:NSManagedObjectContext){
        self.managedContext = moc
    }
    //get issue list
    func getIssueList()->[Issues]?{
        let fetchRequest: NSFetchRequest<Issues> = Issues.fetchRequest()
        let sortDesc = NSSortDescriptor(key: "updated_at", ascending: false)
        fetchRequest.sortDescriptors = [sortDesc]
        do {
            return try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    }
    func saveIssue(issues: [Issue]){
        removeData(entity:"Issues")
        issues.forEach { (issue) in
            let issu = Issues(context: self.managedContext)
            issu.id = issue.id ?? 0
            issu.body = issue.body
            issu.comments_url = issue.comments_url
            issu.title = issue.title
            issu.updated_at = issue.updated_at
        }
        do {
            try self.managedContext.save()
        } catch let error as NSError {
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    }
    func saveComments(comments: [Comment], issueID:Int64){
        removeData(entity:"Comments")
        comments.forEach { (comment) in
            let comm = Comments(context: self.managedContext)
            comm.id = comment.id ?? 0
            comm.body = comment.body
            comm.uesrLogin = comment.user?.login
            comm.updated_at = comment.updated_at
            comm.issueId = issueID
        }
        do {
            try self.managedContext.save()
        } catch let error as NSError {
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    }
    func getCommentList(issueID:Int64)->[Comments]?{
        let fetchRequest: NSFetchRequest<Comments> = Comments.fetchRequest()
        let pred = NSPredicate(format: "issueId == %lld",issueID)
        fetchRequest.predicate = pred
        do {
            return try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    }
    func removeData(entity:String){
        let fetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let delBatchRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try self.managedContext.execute(delBatchRequest)
        } catch let error as NSError {
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    }
}
extension NSManagedObjectContext{
    static var current: NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
