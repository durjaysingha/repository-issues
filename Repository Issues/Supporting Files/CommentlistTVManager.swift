//
//  IssuelistTVManager.swift
//  Repository Issues
//
//  Created by Durjay Singha on 06/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import UIKit
import RxSwift

class CommentlistTVManager: NSObject {
    
    let view: UITableView
    var viewModel: CommentListViewModel
    let selectedIssue = PublishSubject<CommentViewModel>()
    private let disposeBag = DisposeBag()
    
    var issues: [CommentViewModel] {
        return viewModel.existingCommentsVMArray
    }

    func forceRefresh() {
        self.viewModel.reloadData()
    }
    
    init(view: UITableView, commentUrl:String, issueID:Int64) {
        self.view = view
        self.viewModel = CommentListViewModel(commentUrl: commentUrl, issueID:issueID)
        super.init()
        self.view.register(UINib(nibName: "IssueListCell", bundle: nil), forCellReuseIdentifier: "IssueListCell")
        self.view.dataSource = self
        self.view.delegate = self
        self.view.tableFooterView = UIView(frame: .zero)
        self.view.separatorStyle = .none
        setupBindings()
    }
    func setupBindings() {
        self.viewModel.dataReady.asObservable().subscribe { [unowned self] (dataReady) in
            if let _dataReady = dataReady.element,_dataReady {
                self.view.reloadData()
            }
        }.disposed(by: disposeBag)
   
    }
}

extension CommentlistTVManager: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issues.count

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "IssueListCell", for: indexPath) as? IssueListCell {
            cell.issueVM = issues[indexPath.row]
            return cell
        }else{
            return UITableViewCell()

        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIssue.onNext(issues[indexPath.row])
    }
 
   
}
