//
//  UIHelper.swift
//  Repository Issues
//
//  Created by Durjay Singha on 06/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import Foundation
import UIKit
func showAlert(message: String?, duration: Double?=nil, attribute: NSLayoutConstraint.Attribute? = .bottom, attributeConstant: CGFloat?=0, bgColor: UIColor?=nil, txtColor: UIColor?=nil) {
    let keyWindow = UIApplication.shared.connectedScenes
    .filter({$0.activationState == .foregroundActive})
    .map({$0 as? UIWindowScene})
    .compactMap({$0})
    .first?.windows
    .filter({$0.isKeyWindow}).first
    
    keyWindow?.viewWithTag(10101010)?.removeFromSuperview()
    
    let toastMessage = UILabel()
    toastMessage.translatesAutoresizingMaskIntoConstraints = false
    toastMessage.numberOfLines = 0
    toastMessage.lineBreakMode = .byWordWrapping
    toastMessage.textColor = .white
    
    if let _txtColor = txtColor {
        toastMessage.textColor = _txtColor
    }
    
    toastMessage.text = message
    toastMessage.font = UIFont.systemFont(ofSize: 14)
    toastMessage.backgroundColor = .clear
    
    let toastWrapper = UIView()
    toastWrapper.translatesAutoresizingMaskIntoConstraints = false
    toastWrapper.backgroundColor = UIColor.darkGray.withAlphaComponent(0.9)
    
    if let _bgColor = bgColor {
        toastWrapper.backgroundColor = _bgColor.withAlphaComponent(0.9)
    }
    
    toastWrapper.layer.cornerRadius = 5
    toastWrapper.layer.masksToBounds = true
    toastWrapper.tag = 10101010
    
    // add views
    toastWrapper.addSubview(toastMessage)
    keyWindow?.addSubview(toastWrapper)
            
    // wrapper constraint
    NSLayoutConstraint(item: toastWrapper, attribute: attribute ?? .bottom, relatedBy: .equal, toItem: keyWindow, attribute: attribute ?? .bottom, multiplier: 0.9, constant: attributeConstant ?? 0).isActive = true
    NSLayoutConstraint(item: toastWrapper, attribute: .width, relatedBy: .equal, toItem: keyWindow, attribute: .width, multiplier: 0.9, constant: 0).isActive = true
    NSLayoutConstraint(item: toastWrapper, attribute: .centerX, relatedBy: .equal, toItem: keyWindow, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
    
    // label constraint
    NSLayoutConstraint(item: toastMessage, attribute: .leading, relatedBy: .equal, toItem: toastWrapper, attribute: .leading, multiplier: 1, constant: 20).isActive = true
    NSLayoutConstraint(item: toastMessage, attribute: .trailing, relatedBy: .equal, toItem: toastWrapper, attribute: .trailing, multiplier: 1, constant: -20).isActive = true
    NSLayoutConstraint(item: toastMessage, attribute: .top, relatedBy: .equal, toItem: toastWrapper, attribute: .top, multiplier: 1, constant: 15).isActive = true
    NSLayoutConstraint(item: toastMessage, attribute: .bottom, relatedBy: .equal, toItem: toastWrapper, attribute: .bottom, multiplier: 1, constant: -15).isActive = true
    
    UIView.animate(withDuration: duration ?? 2.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastWrapper.alpha = 0.0
        }, completion: { (isCompleted) in
            keyWindow?.viewWithTag(10101010)?.removeFromSuperview()
        }
    )
}
