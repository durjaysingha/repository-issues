//
//  Constant.swift
//  Repository Issues
//
//  Created by Durjay Singha on 05/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

struct Constants {
    struct API {
        static let listURL = "https://api.github.com/repos/firebase/firebase-ios-sdk/issues"
    }
}
