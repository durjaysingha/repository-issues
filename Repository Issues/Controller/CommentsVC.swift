//
//  CommentsVC.swift
//  Repository Issues
//
//  Created by Durjay Singha on 06/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import UIKit
import RxSwift
import SVProgressHUD
class CommentsVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    private let disposeBag = DisposeBag()
    
    var tableViewManager: CommentlistTVManager?
    var commentUrl:String = ""
    var issueID:Int64 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Comments"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.title = "SCREEN_NAME_MY_MEDICAL_HISTORY".localized
        tableViewManager = CommentlistTVManager(view: tableView, commentUrl: commentUrl, issueID:issueID)
        setupBindings()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupBindings() {
        
        self.tableViewManager?.viewModel.dataLoading.asObservable().subscribe { (dataLoading) in
            if let _dataLoading = dataLoading.element,_dataLoading {SVProgressHUD.show()
            }else{
                SVProgressHUD.dismiss()
            }
        }.disposed(by: disposeBag)
        
        self.tableViewManager?.viewModel.issuePublisher.asObservable().subscribe {[unowned self] (msgData) in
            if let msg = msgData.element {
                showAlert(message: msg)
                if msg == "No Commemts Posted"{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }.disposed(by: disposeBag)
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}
