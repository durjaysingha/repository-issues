//
//  ViewController.swift
//  Repository Issues
//
//  Created by Durjay Singha on 05/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import UIKit
import RxSwift
import SVProgressHUD
class IssueListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private let disposeBag = DisposeBag()
    var tableViewManager: IssuelistTVManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "firebase-ios-sdk"
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
    //        self.title = "SCREEN_NAME_MY_MEDICAL_HISTORY".localized
            tableViewManager = IssuelistTVManager(view: tableView)
            setupBindings()
        }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupBindings() {
        self.tableViewManager?.selectedIssue.asObservable().subscribe { [unowned self] (selectedIssue) in
            if let issue = selectedIssue.element {
                self.performSegue(withIdentifier: "dotoDetailPage", sender: issue)
            }
            
        }.disposed(by: disposeBag)
        
        self.tableViewManager?.viewModel.dataLoading.asObservable().subscribe { (dataLoading) in
            if let _dataLoading = dataLoading.element,_dataLoading {
                SVProgressHUD.show(withStatus: "dfsdfsdf")
            }else{
                SVProgressHUD.dismiss()
                
            }
        }.disposed(by: disposeBag)
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let issue = sender as? IssueViewModel{
            let des = segue.destination as! CommentsVC
            des.commentUrl = issue.comments_url
            des.issueID = issue.id
        }
    }
}

