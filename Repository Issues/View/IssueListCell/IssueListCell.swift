//
//  IssueListCell.swift
//  Repository Issues
//
//  Created by Durjay Singha on 05/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import UIKit

class IssueListCell: UITableViewCell {
  
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bodyLbl: UILabel!
    var issueVM: Any?{
        didSet{
            if let iue = issueVM as? IssueViewModel{
                titleLbl.text = iue.title
                bodyLbl.text = iue.body
                bodyLbl.textAlignment = .left
            }else if let iue = issueVM as? CommentViewModel{
                titleLbl.text = iue.body
                bodyLbl.text = iue.userLogin
                bodyLbl.textAlignment = .right

            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.red
        container.layer.cornerRadius = 10.0
        container.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        container.layer.masksToBounds = true
        container.layer.shadowOffset = CGSize(width: 2, height: 10)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
