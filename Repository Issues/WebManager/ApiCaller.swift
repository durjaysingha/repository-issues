//
//  ApiCaller.swift
//  Repository Issues
//
//  Created by Durjay Singha on 06/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import UIKit
import SVProgressHUD

enum Method {
    case post
    case get
}
enum ServiceType{
    case issueList
    case commentList
}
class ApiCaller: NSObject {
    
    static let shared = ApiCaller()
    
    private override init() {
        print("hello init")
    }
    
    func postData(apiUrl:String, params: [String: Any], type:ServiceType, completion: ((Any?) -> ())?) {
        guard let url = URL(string: apiUrl) else {return}
        var request = URLRequest(url: url)
        request.cachePolicy = .reloadIgnoringLocalCacheData
        request.httpMethod = "GET"
        let config = URLSessionConfiguration.ephemeral
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.timeoutIntervalForRequest = 30
        
        let session = URLSession(configuration: config, delegate: nil, delegateQueue: nil)
        
        DispatchQueue.global(qos: .background).async {
            session.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        if let callback = completion {
                            callback(error)
                        }
                    } else {
                        if let _data = data,let responseString = String(data: _data, encoding: .utf8) {
                            print(responseString)
                            if let callback = completion {
                                if type == .issueList{
                                    do {
                                        let issueData = try JSONDecoder().decode([Issue].self, from: _data)
                                        if issueData.count==0{
                                            callback("No Issues Posted")
                                            return
                                        }
                                        callback(issueData)
                                    } catch let error as NSError{
                                        callback(error.localizedDescription)
                                    }
                                } else {
                                    do {
                                        let issueData = try JSONDecoder().decode([Comment].self, from: _data)
                                        if issueData.count==0{
                                            callback("No Commemts Posted")
                                            return
                                        }
                                        callback(issueData)
                                    } catch let error as NSError{
                                        callback(error.localizedDescription)
                                    }
                                }
                                
                            }
                        }else{
                            
                            if let callback = completion {
                                callback(nil)

                            }
                        }
                    }
                }
            }.resume()
        }
    }
    
  
}


extension Dictionary {
   var queryString: String {
      var output: String = ""
      for (key,value) in self {
          output +=  "\(key)=\(value)&"
      }
      output = String(output.dropLast())
      return output
   }
}
