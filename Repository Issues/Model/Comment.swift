//
//  RI_Issue.swift
//  Repository Issues
//
//  Created by Durjay Singha on 05/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import Foundation

// MARK: - Issue

class Comment: Codable {
    let user: User?
    let id: Int64?
    let updated_at: String?
    let body: String?
    
    enum CodingKeys: String, CodingKey {
        case user = "user"
        case id = "id"
        case updated_at = "updated_at"
        case body = "body"
    }
}
class User: Codable {
    let login: String?
   enum CodingKeys: String, CodingKey {
        case login = "login"
    }
}
