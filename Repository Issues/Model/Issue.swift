//
//  RI_Issue.swift
//  Repository Issues
//
//  Created by Durjay Singha on 05/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import Foundation

// MARK: - Issue

class Issue: Codable {
    let title: String?
    let id: Int64?
    let comments_url: String?
    let updated_at: String?
    let body: String?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case id = "id"
        case comments_url = "comments_url"
        case updated_at = "updated_at"
        case body = "body"
    }
}
