//
//  IssueViewModel.swift
//  Repository Issues
//
//  Created by Durjay Singha on 06/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

import Foundation
class CommentViewModel {
    let userLogin: String?
    let id: Int64?
    let updated_at: String?
    let body: String?
    
    init(model:Comments) {
        userLogin = model.uesrLogin ?? ""
        updated_at = model.updated_at ?? ""
        body = model.body ?? ""
        id = model.id
        
    }
}
