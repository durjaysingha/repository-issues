//
//  IssueListViewModel.swift
//  Repository Issues
//
//  Created by Durjay Singha on 06/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//
import UIKit
import RxSwift
class IssueListViewModel {
    private var existingIssues = [Issues]() {
        didSet {
            for issue in self.existingIssues {
                self.existingIssuesVMArray.append(IssueViewModel(model:issue))
            }
        }
    }
    var existingIssuesVMArray = [IssueViewModel]()
    var dataReady = PublishSubject<Bool>()
    var dataLoading = PublishSubject<Bool>()
    
    init(forcePullData: Bool = true) {
        if let issueList = DataManager.shared.getIssueList(){
            self.existingIssues = issueList
            
        }
        
        for issue in self.existingIssues {
            self.existingIssuesVMArray.append(IssueViewModel(model:issue))
        }
        
        
        if forcePullData {
            self.dataReady.onNext(true)
            self.dataReady.onNext(false)
            reloadData()
            print("Pulling condition")
        } else {
            
            self.dataReady.onNext(true)
        }
    }
    private func getData(){
        if let issueList = DataManager.shared.getIssueList(){
            self.existingIssues = issueList
            self.dataReady.onNext(true)
        }
    }
    func reloadData(){
        self.existingIssuesVMArray.removeAll()
        self.dataLoading.onNext(true)
        if let apiCallDate = UserDefaults.standard.value(forKey: "apiCallDate") as? Date{
            let date = Date()
            let diffComponents = Calendar.current.dateComponents([.second], from: apiCallDate, to: date)
            if let seconds = diffComponents.second, seconds<86400{
                self.getData()
                return
            }
        }
        ApiCaller.shared.postData(apiUrl: Constants.API.listURL , params: [:], type: .issueList) { issues in
            if let _issues = issues as? [Issue] {
                UserDefaults.standard.set(Date(), forKey: "apiCallDate")
                self.dataLoading.onNext(false)
                DataManager.shared.saveIssue(issues: _issues)
                self.getData()
            }else{
                self.getData()
            }
            
        }
    }
    
}
