//
//  IssueListViewModel.swift
//  Repository Issues
//
//  Created by Durjay Singha on 06/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//
import UIKit
import RxSwift
class CommentListViewModel {
    private var existingComments = [Comments]() {
        didSet {
            for comment in self.existingComments {
                self.existingCommentsVMArray.append(CommentViewModel(model:comment))
            }
        }
    }
    
    var existingCommentsVMArray = [CommentViewModel]()
    var dataReady = PublishSubject<Bool>()
    var dataLoading = PublishSubject<Bool>()
    var issuePublisher = PublishSubject<String>()
    var commentUrl: String
    var issueID: Int64
    
    init(forcePullData: Bool = true, commentUrl:String = "", issueID:Int64 = 0) {
        self.commentUrl = commentUrl
        self.issueID = issueID
        
        if let commetns = DataManager.shared.getCommentList(issueID: self.issueID){
            self.existingComments = commetns
        }
        
        for comment in self.existingComments {
            self.existingCommentsVMArray.append(CommentViewModel(model:comment))
        }
        
        if forcePullData {
            self.dataReady.onNext(true)
            self.dataReady.onNext(false)
            reloadData()
            print("Pulling condition")
        } else {
            
            self.dataReady.onNext(true)
        }
    }
    private func getData(issueID:Int64){
        if let comments = DataManager.shared.getCommentList(issueID:self.issueID){
            self.existingComments = comments
            self.dataReady.onNext(true)
        }
    }
    func reloadData(){
        self.existingCommentsVMArray.removeAll()
        self.dataLoading.onNext(true)
        ApiCaller.shared.postData(apiUrl: self.commentUrl, params: [:], type: .commentList) { comments in
            self.dataLoading.onNext(false)
            if let _comments = comments as? [Comment],_comments.count>0 {
                DataManager.shared.saveComments(comments: _comments, issueID: self.issueID)
                self.getData(issueID: self.issueID)
            }else{
                if let error = comments as? String{
                      self.issuePublisher.onNext(error)
                }
             
                self.getData(issueID: self.issueID)
            }
            
        }
    
    }
    
}
