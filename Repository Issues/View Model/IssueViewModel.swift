//
//  IssueViewModel.swift
//  Repository Issues
//
//  Created by Durjay Singha on 06/01/21.
//  Copyright © 2021 Durjay Singha. All rights reserved.
//

class IssueViewModel {
    let title: String
    let comments_url: String
    let updated_at: String
    let body: String
    let id: Int64
    
    init(model:Issues) {
        title = model.title ?? ""
        comments_url = model.comments_url ?? ""
        updated_at = model.updated_at ?? ""
        body = (model.body ?? "").getMaxCharacters()
        id = model.id
        
    }
}
extension String{
    func getMaxCharacters()->String{
        return  self.count<0 ? self : String(self.prefix(140))
    }
}
